// import axios from 'axios'
import { useSelector } from 'react-redux';
import './App.css';
// import React, {useState, useEffect} from 'react';
// import {increment} from './redux/action/countAction';
import Login from './Login'
import Navbar from './Navbar'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import ListCar from './Listcar/ListCar';
import Dropzone from 'react-dropzone-uploader'
import 'react-dropzone-uploader/dist/styles.css'
import {useState, useEffect} from 'react';
import { Bar } from 'react-chartjs-2';
// eslint-disable-next-line
import { Chart } from 'chart.js/auto';
import Form from './Form';

function App() {
  const [files, setFiles] = useState();
  const [dataCar, setDataCar] = useState([]);

  const getDataCar = async () => {
    const data = await fetch(`https://rent-cars-api.herokuapp.com/admin/car`);
    const result = await data.json();
    setDataCar(result);
    console.log(result);
  }
  const getUploadParams = ({ meta }) => {
    setFiles(meta)
    return { url: 'https://httpbin.org/post' }
  };

  const handleChangeStatus = ({ meta, file }, status) => { 
    console.log(`Perubahan terjadi ${status}`) 
    setFiles(meta)
  };

  const handleSubmit = (files, allFiles) => {
    console.log(`Files => ${files} && all Files => ${allFiles}`)
    allFiles.forEach((f) => f.remove())
  };

  useEffect(() => {
    getDataCar();
  }, [files]) 

  const dataChart = {
    labels: ['January', 'February', 'March'],
    datasets: [
      {
        label: 'terjual',
        backgroundColor: "purple",
        data: dataCar.map((item) => item.price),
      },
      {
        label: 'tidak terjual',
        data: [3, 5, 61],
        backgroundColor: "blue",
      },
    ]
  }

  const globalStore = useSelector((state) => state.countReducer)
  console.log(globalStore)
  // const {nama, counter} = useSelector((state) => state.countReducer)
  const {dataProfile} = useSelector((state) => state.authReducer)
  console.log(dataProfile)
  // const dispatch = useDispatch()
  // const [dataCar, setDataCar] = useState()

  // useEffect(() => {
  //   axios('https://rent-cars-api.herokuapp.com/admin/car').then((resultFromAxios) => console.log(resultFromAxios))
  // }, [])

  return (
    <div className="App">
      {/* {counter}
      <button onClick={() => dispatch(increment(counter + 1))}>Tambah</button> */}
      <h1>Selamat Datang!</h1>
      {/* Data Visualisasi with react chart.js 2*/}
      <div sytle={{width: 900}}>
        <Bar data={dataChart} redraw={true} />
      </div>
      <Dropzone
      getUploadParams={getUploadParams}
      onChangeStatus={handleChangeStatus}
      onSubmit={handleSubmit}
      accept="image/*,audio/*,video/*"
      />
      <ListCar/>
      <Form CALL_FUNCTION={() => console.log("dammy function")} />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/dashboard" element={<Navbar />} />
          <Route path="*" element={<h1>Halaman Not Found</h1>} />
        </Routes>
      </BrowserRouter> 
      
    </div>
  );
}

export default App;
