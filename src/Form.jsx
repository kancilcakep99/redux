import React from 'react'

const Form = ({CALL_FUNCTION}) => {
  return (
    <>
    <h1 data-testid="text-form">Halaman form</h1>
    <p>Selamat data di Halaman form</p>
    <button data-testid="btnSubmit" onClick={() => CALL_FUNCTION()}>Submit</button>
    </>
  )
}

export default Form