import React from 'react'
import axios from 'axios'
import { useState } from 'react'

const ListCar = () => {
    const [listCar, setListCar] = useState([])
    const getCar = async () => {
        const {data} = await axios.get(
            `https://rent-cars-api.herokuapp.com/admin/car`
        );
        console.log(data)
        setListCar(data)
    }; 

  return (
    <>
        <h1>List Car</h1>
        <button data-testid="btn-Show" onClick={getCar}>Show list</button>
        <ul>
            {listCar.map((car) => (
                <li key={car.id} data-testid="car">{car.name}</li>
            ))}
        </ul>
    </>
  )
}

export default ListCar