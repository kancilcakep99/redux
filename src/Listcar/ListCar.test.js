import { render, screen, waitFor } from "@testing-library/react";
import axios from "axios";
import ListCar from "./ListCar";
import userEvent from "@testing-library/user-event";


jest.mock("axios")

test("Apakah data dari api jumlahnya sama", async () => {
    const datalistCar = [
        {
            id: 1,
            name: 'Kijang',
        },
        {
            id: 2,
            name: 'Inova',
        },
        {
            id: 3,
            name: 'Fortuner',
        }
    ];

    axios.get.mockImplementation(() => Promise.resolve({ data: datalistCar }));
    render(<ListCar />) 

    const btnShow = screen.getByTestId("btn-Show");
    userEvent.click(btnShow);
    await waitFor(async () => {
        const listCar = screen.getAllByTestId("car");
        expect(listCar).toHaveLength(datalistCar.length);
    })
})