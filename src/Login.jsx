import React from 'react'
// import Axios  from 'axios'
import { useDispatch } from 'react-redux'
import { LoginAction } from './redux/action/authAction'
import { useNavigate } from 'react-router-dom'
const Login = () => {

    const dispatch = useDispatch()
    const navigate = useNavigate()
    const reqLogin = () => {
      dispatch(LoginAction());
      navigate('/dashboard')
    }
    // const reqLogin = async () => {
    //     const {data} = await Axios.post(`https://rent-cars-api.herokuapp.com/admin/auth/login`, {
    //         email: "admin@mail.com", 
    //         password: "123456",
    //     }
    //     );
    //     dispatch({type: "SET_PROFILE_LOGIN", payload: data})
    // }

  return (
    <>
        <h1>Halaman Login</h1>
        <button onClick={reqLogin}>Login</button>
    </>
  )
}

export default Login