import React from 'react'
import { useSelector } from 'react-redux'


const Navbar = () => {
    const {dataProfile} = useSelector((globalStore) => globalStore.authReducer)

    return  <h1>Selamat Datang {dataProfile?.role} </h1>
}

export default Navbar