const penjumlahan = (angka1, angka2) => {
    return angka1 + angka2;
}

// pengurangan
const pengurangan = (angka1, angka2) => {
    return angka1 - angka2;
}

// pembagian
const pembagian = (angka1, angka2) => {
    return angka1 / angka2;
}

// perkalian
const perkalian = (angka1, angka2) => {
    return angka1 * angka2;
}

// rata-rata
const mean = (angka1, angka2) => {
    return (angka1 + angka2)/2;
}

// implementasi test react app basic
test('should be sum', () => {
    expect(2 + 2).toBe(4);
});

// melakukan grouping dr masing-masing test dijadikan satu group
describe('testing for aritmatika', () => {
    test('tambah', () => {
        expect(penjumlahan(1, 1)).toBe(2);
    });
    
    it('kurang', () => {
        expect(pengurangan(3, 1)).toBe(2);
    });
    
    test('bagi', () => {
        expect(pembagian(6, 2)).toBe(3);
    });
    
    it('kali', () => {
        expect(perkalian(5, 2)).toBe(10);
    });
    
    test('rata-rata', () => {
        expect(mean(6, 2)).toBe(4);
    });
})
