import {render, screen} from '@testing-library/react';
import Header from './Header';

test('should be render Header', () => {
    render(<Header/>);
    expect(screen.getByText(/Header/i)).toBeInTheDocument();
})