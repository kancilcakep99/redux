import Axios  from "axios";
export const LoginAction = () => {
    return async (dispatch) => {
        const {data} = await Axios.post(`https://rent-cars-api.herokuapp.com/admin/auth/login`, 
            {
                email: "admin@mail.com", 
                password: "123456",
            }
        );
        dispatch({type: "SET_PROFILE_LOGIN", payload: data})
    }
}