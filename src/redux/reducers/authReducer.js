const initialState = {
    dataProfile: null
}

const authReducer = (state = initialState, action) => {
    switch(action.type){
        case "SET_PROFILE_LOGIN":
            return {
                ...state, 
                dataProfile: action.payload
            }
            default: return state
    }
}

export default authReducer;