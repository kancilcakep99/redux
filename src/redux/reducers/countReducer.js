const initialState = {
    nama: '',
    counter: 0,
}

// reducer
const countReducer = (state = initialState, action) => {
    switch(action.type){
        case "SET_COUNTER":
            return {
                ...state,
                counter: action.payload,
            }
            default: {
                return state
            }
    }
}

export default countReducer;